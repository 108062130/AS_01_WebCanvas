var canvas = null;
var ctx = null;
var size = 5;
var color = null;
var state = 'pen';
var textin = null;
var wordsize = null;
var hasinput = null;
var wordtype = null;
var newwordsize = null;
var wdstate = 'sans-serif';


function mouse(canvas,evt){
    var r = canvas.getBoundingClientRect();
    return{
        x:evt.clientX - r.left,
        y:evt.clientY - r.top
    };
}
function move(evt){
    var p = mouse(canvas,evt);
    if(state == 'pen')
    {
        ctx.lineCap = 'round';
        ctx.lineTo(p.x,p.y);
        ctx.stroke();
        ctx.moveTo(p.x,p.y);
    }
    else if(state == 'eraser')
    {
        ctx.lineTo(p.x,p.y);
        ctx.clearRect( p.x-size.value/2, p.y-size.value/2, size.value, size.value);
        ctx.moveTo(p.x,p.y);
    }
    else if(state == 'text')
    {
        canvas.onclick = function(p){
            if(hasinput) return;
            addinput(p.x,p.y);
        }
    }
}

function init(){
    canvas = document.getElementById('mycanvas');
    ctx = canvas.getContext('2d');
    hasinput = false;

    canvas.addEventListener('mousedown',function(evt){
        var p = mouse(canvas,evt);
        ctx.beginPath();
        ctx.moveTo(p.x,p.y);
        evt.preventDefault();
        canvas.addEventListener('mousemove',move,false);
    });
    
    canvas.addEventListener('mouseup',function(){
        canvas.removeEventListener('mousemove',move,false);
        stopdraw()
    },false);

    size = document.getElementById('size');
    size.addEventListener('change',function(){
        ctx.lineWidth = size.value;
    }, false);

    wordsize = document.getElementById('wordsize');
    wordsize.addEventListener('change',function(){
    }, false);

    color = document.getElementById('pencolor');
    color.addEventListener('change',function(){
        ctx.strokeStyle = color.value;
    }, false);

    document.getElementById('download').addEventListener('click',function(){
        var link = document.createElement('a');
        link.download = "";
        link.href = canvas.toDataURL()
        link.click();
        link.delete;
    }, false);

    document.getElementById('upload').addEventListener('change',function(e){
        var reader = new FileReader();
        reader.onload = function(event){
            var img = new Image();
            img.onload = function(){
                canvas.width = img.width;
                canvas.height = img.height;
                ctx.drawImage(img,0,0);
            }
            img.src = event.target.result;
        }
        reader.readAsDataURL(e.target.files[0]);  
    }, false);

    document.getElementById('reset').addEventListener('click',function(){
        var r = confirm("reset?");
        if(r == true){ctx.clearRect(0,0,500,500);}
    }, false);

    cursor();

    let history = ctx.getImageData(0,0,mycanvas.width,mycanvas.height);
    window.history.pushState(history,null);

    document.getElementById('undo').addEventListener('click',function(){
        window.history.go(-1);
    }, false);

    document.getElementById('redo').addEventListener('click',function(){
        window.history.go(1);
    }, false);

    window.addEventListener('popstate',changeStep,false);
}

function addinput(x,y){
    textin = document.createElement('input');
    textin.type = 'text';
    textin.style.position = 'fixed';
    textin.style.left = (x-4) + 'px';
    textin.style.top = (y-4)+ 'px';
    textin.onkeydown = handleEnter;
    document.body.appendChild(textin);
    textin.focus();
    hasinput = true;
}

function handleEnter(e){
    var keycode = e.keyCode;
    if(keycode === 13)
    {
        drawtext(this.value,parseInt(this.style.left,10),parseInt(this.style.top,10));
        document.body.removeChild(this);
        hasinput = false;
    }
}

function drawtext(txt,x,y){
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = wordsize.value + 'px ' + wdstate;
    ctx.fillText(txt,x-4,y-4);
}

function cursor(val)
{
    document.getElementById('pen_2').setAttribute("value",val);
}

function stopdraw(e)
{
    let history = ctx.getImageData(0,0,mycanvas.width,mycanvas.height);
    window.history.pushState(history,null);
}

function changeStep(e){
    ctx.clearRect(0,0,mycanvas.width,mycanvas.height);
    if(e.state){
        ctx.putImageData(e.state,0,0);
    }
}

function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}

