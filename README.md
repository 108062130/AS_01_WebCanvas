# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | N         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---


### Function description

![](https://i.imgur.com/xhZdPgr.png)<br>

First we have to solve the operations of mouse,using the same idea that starts drawing when mousedown then move it with mouse's moveing.Finally stop drawing when mouseup.<br>
![](https://i.imgur.com/zeJ3oSv.png)<br>
The key point is the "move" function.<br>
![](https://i.imgur.com/IOdsJ8i.png)<br>
Using state to control what tool is using and what to do.<br>
* Brush<br>
![](https://i.imgur.com/ccACmza.png)<br>
Brush=>draw round at the endpoint when it moves then we will get lines.<br>
![](https://i.imgur.com/ykLr8dO.png)<br>
When color is changed,change the color of stroke.<br>
![](https://i.imgur.com/NVd16be.png)<br>
When size is changed,change the size of stroke.<br>
* Eraser<br>
![](https://i.imgur.com/BVdYTy6.png)<br>
Eraser=>clear the element in the position of mouse. <br>
* Text<br>
![](https://i.imgur.com/dyN9bC7.png)<br>
Text<br>
![](https://i.imgur.com/QifglnN.png)<br>
First we load the text input.<br>
![](https://i.imgur.com/cotnDOe.png)<br>
We end up loading the input when a enter shows up.<br>
Then take a look at choosing font type and size.<br>
![](https://i.imgur.com/pbrzrab.png)<br>
We save font type and size.When we wants to draw text,we load font type and size everytime.<br>

* Simple menu & mouse icon<br>
![](https://i.imgur.com/uaaazgf.png)<br>
Give every buttons id and value and they are clicked change the state mention behind.Set the buttons' position and cursor icon by css.<br>
![](https://i.imgur.com/aMzWtAe.png)<br>
To change the cursor on the canvas<br>
![](https://i.imgur.com/FvdRstB.png)<br>
(Only take pen(brush) for example,the rest are the same)<br>

* Refresh button<br>
![](https://i.imgur.com/AQe3vxr.png)<br>
When reset are clicked first confirm that whether you want to reset or not.Then if you chooce yes,clear the canvas as the same function of eraser,but clear the whole canvas.<br>

* Undo & Redo<br>
![](https://i.imgur.com/W3deGkh.png)<br>
When we do any input we save it into histroy.<br>
![](https://i.imgur.com/lxteSI6.png)<br>
When undo is clicked go for the previous one histroy.<br>
![](https://i.imgur.com/Yns4XfP.png)<br>
When undo is clicked go for the next one histroy.<br>

* Image<br>
![](https://i.imgur.com/N1UKvZK.png)<br>
First we create a button for upload by input type=file<br>
![](https://i.imgur.com/FgQgyUv.png)<br>
Then we put the image and change the size of canvas.<br>

* Download<br>
![](https://i.imgur.com/OUsdFei.png)<br>
Create a element and download it.<br>

* Different Brush shapes<br>
They only have buttons,but the buttons are useless.<br>

### Gitlab page link

https://108062130.gitlab.io/AS_01_WebCanvas

<style>
table th{
    width: 100%;
}
</style>
